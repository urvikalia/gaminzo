package com.thoughtworks.vapasi.gaminzo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GaminzoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GaminzoApplication.class, args);
	}

}
