package com.thoughtworks.vapasi.gaminzo.validation;

import com.thoughtworks.vapasi.gaminzo.repository.GaminzoUserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail,String> {


    @Autowired
    private GaminzoUserRepository gaminzoUserRepository;
    @Override
    public void initialize(UniqueEmail constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return email!=null && gaminzoUserRepository.findByUsername(email)==null;
    }
}
