package com.thoughtworks.vapasi.gaminzo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordPatternValidator implements ConstraintValidator<PasswordPattern,String> {

    @Override
    public void initialize(PasswordPattern constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return  value.length()>=8 && value.length()<16 && value.matches("[A-za-z0-9@#$%!^*]+");
    }
}
