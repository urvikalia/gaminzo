package com.thoughtworks.vapasi.gaminzo.repository;

import com.thoughtworks.vapasi.gaminzo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {

    Product findByName(String name);

    List<Product> findByQuantityGreaterThanOrderByCreationDateDesc(long quantity);
}
