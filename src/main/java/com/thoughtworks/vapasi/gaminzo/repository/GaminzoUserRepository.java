package com.thoughtworks.vapasi.gaminzo.repository;

import com.thoughtworks.vapasi.gaminzo.model.GaminzoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GaminzoUserRepository extends JpaRepository<GaminzoUser,Long> {

        GaminzoUser findByUsername(String username);
}
