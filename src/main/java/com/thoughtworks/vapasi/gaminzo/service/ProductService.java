package com.thoughtworks.vapasi.gaminzo.service;

import com.thoughtworks.vapasi.gaminzo.model.Product;
import com.thoughtworks.vapasi.gaminzo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {


    @Autowired
    ProductRepository productRepository;


    public List<Product> getProducts()
    {
        return productRepository.findAll();
    }

    public Optional<Product> getProduct(long id)
    {
        return productRepository.findById(id);
    }

    public Product getProductByName(String name)
    {
        return productRepository.findByName(name);
    }

    public List<Product> getActiveProducts()
    {
        return  productRepository.findByQuantityGreaterThanOrderByCreationDateDesc(0);
    }
}
