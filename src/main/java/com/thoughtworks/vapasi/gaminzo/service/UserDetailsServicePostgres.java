package com.thoughtworks.vapasi.gaminzo.service;

import com.thoughtworks.vapasi.gaminzo.model.GaminzoUser;
import com.thoughtworks.vapasi.gaminzo.repository.GaminzoUserRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServicePostgres implements UserDetailsService {

    @Autowired
    private GaminzoUserRepository gaminzoUserRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        GaminzoUser user=gaminzoUserRepository.findByUsername(username);
        if(user==null)
            throw new UsernameNotFoundException(username);
        return User.withUsername(user.getUsername()).password(user.getPassword()).roles(user.getRole()).build();
    }





}
