package com.thoughtworks.vapasi.gaminzo.service;

import com.thoughtworks.vapasi.gaminzo.model.Cart;
import com.thoughtworks.vapasi.gaminzo.model.CartItem;
import com.thoughtworks.vapasi.gaminzo.model.CartItemDto;
import com.thoughtworks.vapasi.gaminzo.model.Product;
import com.thoughtworks.vapasi.gaminzo.repository.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartItemService {

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private GaminzoUserService gaminzoUserService;

    @Autowired
    private ProductService productService;

    public CartItem addItemToCart(CartItem item)
    {
        return cartItemRepository.saveCartItem(item);
    }

    public void removeItemFromCart(CartItem item)
    {
        cartItemRepository.delete(item);
    }

    public void clearCart(long userId)
    {
        cartItemRepository.deleteAllByUserId(userId);
    }


    public Cart getCart() {
        Cart cart=new Cart();
        UserDetails user=gaminzoUserService.fetchUserDetails();
        cart.setUserName(user.getUsername());
        List<CartItem> cartItems=cartItemRepository.findByUserId( gaminzoUserService.fetchUserIdFromName());
        for(CartItem cartItem:cartItems)
        {
            cartItem.getProductId();
            Optional<Product> product=productService.getProduct(cartItem.getProductId());
            if(product.isPresent()) {
                cart.addCartItem(new CartItemDto(product.get(),cartItem));
            }
        }
        return cart;
    }
}
