package com.thoughtworks.vapasi.gaminzo.model;

import com.thoughtworks.vapasi.gaminzo.validation.PasswordPattern;
import com.thoughtworks.vapasi.gaminzo.validation.UniqueEmail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor

public class RegisterUserDto {

    @NotEmpty(message = "Please enter email")
    @Email(message = "Email should be valid")
    @UniqueEmail
    private String username;

    @NotEmpty(message = "Please enter password")
    @PasswordPattern(message ="Invalid Password")
    private String password;

    private String role="user";

    @NotEmpty(message = "Password doesn't Match")
    private String confirmPassword;

}
