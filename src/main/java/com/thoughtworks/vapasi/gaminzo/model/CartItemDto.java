package com.thoughtworks.vapasi.gaminzo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartItemDto {

    private long productId;
    private String productName;
    private long MaxQuantity;
    private long price;
    private String imageUrl;
    // can be referred with Product Object directly

    private long cartId;
    private long count;


    public CartItemDto(Product prd, CartItem cartItem)
    {
        this.setProductId(prd.getId());
        this.setProductName(prd.getName());
        this.setMaxQuantity(prd.getQuantity());
        this.setPrice(prd.getPrice());
        this.setImageUrl(prd.getImageUrl());
        this.setCartId(cartItem.getId());
        this.setCount(cartItem.getCount());
    }

}
