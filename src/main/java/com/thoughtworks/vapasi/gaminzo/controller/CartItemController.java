package com.thoughtworks.vapasi.gaminzo.controller;

import com.thoughtworks.vapasi.gaminzo.model.Cart;
import com.thoughtworks.vapasi.gaminzo.model.CartItem;
import com.thoughtworks.vapasi.gaminzo.model.CartItemDto;
import com.thoughtworks.vapasi.gaminzo.model.GaminzoUser;
import com.thoughtworks.vapasi.gaminzo.service.CartItemService;
import com.thoughtworks.vapasi.gaminzo.service.GaminzoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/gaminzo/cart")
public class CartItemController {

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private GaminzoUserService gaminzoUserService;

    @GetMapping("/addtocart/{productid}")
    public String  addItemToCart(@PathVariable("productid") long productId)
    {
        CartItem item = new CartItem(gaminzoUserService.fetchUserIdFromName(),productId,1);
         cartItemService.addItemToCart(item);
         return "redirect:/gaminzo/products";
    }


    @GetMapping("/removefromcart/{productid}")
    public void removeFromCart(@PathVariable("productid") long productId)
    {
        CartItem item = new CartItem(gaminzoUserService.fetchUserIdFromName(),productId,1);
        cartItemService.removeItemFromCart(item);
    }

    @GetMapping
    public String getCart(Model model)
    {
        Cart cart=cartItemService.getCart();
        model.addAttribute("cart",cart);
        return "cart";

    }



}
