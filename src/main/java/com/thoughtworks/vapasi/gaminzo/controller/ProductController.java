package com.thoughtworks.vapasi.gaminzo.controller;

import com.thoughtworks.vapasi.gaminzo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/gaminzo/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping()
    public String getProducts(Model model)
    {
        model.addAttribute("productLst",productService.getActiveProducts());
        return "product";
    }
}
