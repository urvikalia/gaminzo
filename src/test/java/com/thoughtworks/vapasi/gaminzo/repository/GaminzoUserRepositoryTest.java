package com.thoughtworks.vapasi.gaminzo.repository;

import com.thoughtworks.vapasi.gaminzo.model.GaminzoUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
public class GaminzoUserRepositoryTest {

    @Autowired
    GaminzoUserRepository gaminzoUserRepository;

    @Autowired
    private TestEntityManager manager;


    @Test
    public void shouldAddUser()
    {
        GaminzoUser savedUser=gaminzoUserRepository.save(new GaminzoUser("Urvi","password","admin"));
        Assertions.assertNotNull(manager.find(GaminzoUser.class,savedUser.getId()));
    }

    @Test
    public void shouldFetchSpecifiedUser()
    {
        GaminzoUser savedUser =manager.persist(new GaminzoUser("urvikalia@gmail.com","password","admin"));
        Assertions.assertNotNull(gaminzoUserRepository.findByUsername(savedUser.getUsername()));
        Assertions.assertEquals(savedUser,gaminzoUserRepository.findByUsername(savedUser.getUsername()));

    }


}
