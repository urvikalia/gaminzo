package com.thoughtworks.vapasi.gaminzo.repository;

import com.thoughtworks.vapasi.gaminzo.model.CartItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

@DataJpaTest
public class CartItemRepositoryTest {

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    TestEntityManager manager;

    @Test
    public void shouldSaveCartItem()
    {
        CartItem cartItem= new CartItem();
        CartItem savedCartItem =cartItemRepository.save(cartItem);
        Assertions.assertNotNull(manager.find(CartItem.class,savedCartItem.getId()));
    }

    @Test
    public void shouldFindAllItemsForSpecificUser()
    {
        CartItem cartItem1= new CartItem(1,2,2);
        CartItem cartItem2= new CartItem(1,3,5);
        CartItem cartItem3= new CartItem(2,4,2);

        manager.persist(cartItem1);
        manager.persist(cartItem2);
        manager.persist(cartItem3);

        Assertions.assertEquals(2,cartItemRepository.findByUserId(1).size());

    }

    @Test
    public void shouldFindCartItemForSpecifiedUserAndProductName()
    {
        manager.persist(new CartItem(1,2,2));
        manager.persist(new CartItem(1,3,5));
        manager.persist(new CartItem(2,4,2));

        Assertions.assertEquals(5, cartItemRepository.
                findByUserIdAndProductId(1,3).get().getCount());
    }

    @Test
    public void shouldAddMultipleItems()
    {
        CartItem cartItem1= new CartItem(1,2,1);
        CartItem item = cartItemRepository.saveCartItem(cartItem1);
        CartItem cartItem2= new CartItem(1,2,1);
        CartItem item1 = cartItemRepository.saveCartItem(cartItem2);

        Assertions.assertEquals(2, manager.find(CartItem.class, item.getId())
                .getCount());
    }

    @Test
    public void shouldDeleteAllItemsForSpecificUser()
    {
        manager.persist(new CartItem(1,2,2));
        manager.persist(new CartItem(1,3,5));
        manager.persist(new CartItem(2,4,2));

        cartItemRepository.deleteAllByUserId(1);

        Assertions.assertEquals(0, cartItemRepository.findByUserId(1).size());

    }

}
