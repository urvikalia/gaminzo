package com.thoughtworks.vapasi.gaminzo.repository;

import com.thoughtworks.vapasi.gaminzo.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;


@DataJpaTest
class ProductRepositoryTest {

    @Autowired
    private
    ProductRepository productRepository;

    @Autowired
    private TestEntityManager manager;

    @Test
    public void shouldAddProduct()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product prd2=new Product("Pictionary",15,600,"http://imagerepo.pictionary.jpg","pictionary");

        Product saved_prd1=productRepository.save(prd1);
        Product saved_prd2=productRepository.save(prd2);

        Assertions.assertNotNull(manager.find(Product.class,prd1.getId()));
        Assertions.assertNotNull(manager.find(Product.class,prd2.getId()));
    }


    @Test
    public void shouldReturnAllProducts()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product prd2=new Product("Pictionary",15,600,"http://imagerepo.pictionary.jpg","pictionary");
        Product prd3=new Product("Head band",0,600,"http://imagerepo.headband.jpg","headband");

        manager.persist(prd1);
        manager.persist(prd2);
        manager.persist(prd3);
        Assertions.assertEquals(3,productRepository.findAll().size());
    }

    @Test
    public void shouldReturnAllProductsWithQuantityMoreThanZeroAndOrdered()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product prd2=new Product("Pictionary",15,600,"http://imagerepo.pictionary.jpg","pictionary");
        Product prd3=new Product("Head band",14,600,"http://imagerepo.headband.jpg","headband");

        Product saved_prd1=manager.persist(prd1);
        Product saved_prd2=manager.persist(prd2);
        Product saved_prd3=manager.persist(prd3);

        saved_prd1.getCreationDate().setDate(1);
        saved_prd2.getCreationDate().setDate(10);
        saved_prd3.getCreationDate().setDate(15);

        manager.persist(saved_prd1);
        manager.persist(saved_prd2);
        manager.persist(saved_prd3);

        List<Product> orderProductList=productRepository.findByQuantityGreaterThanOrderByCreationDateDesc(0);

        Assertions.assertEquals(3,orderProductList.size());


      Assertions.assertEquals(saved_prd3.getName(),orderProductList.get(0).getName());
      Assertions.assertEquals(saved_prd2.getName(),orderProductList.get(1).getName());
      Assertions.assertEquals(saved_prd1.getName(),orderProductList.get(2).getName());

    }

    @Test
    public void shouldReturnSpecificProductForId()
    {

        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product prd2=new Product("Pictionary",15,600,"http://imagerepo.pictionary.jpg","pictionary");

        Product saved_prd1=manager.persist(prd1);
        Product saved_prd2=manager.persist(prd2);

        Assertions.assertEquals(saved_prd1,productRepository.findById(saved_prd1.getId()).get());
        Assertions.assertEquals(saved_prd2,productRepository.findById(saved_prd2.getId()).get());

    }

    @Test
    public void shouldReturnProductForName()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product prd2=new Product("Pictionary",15,600,"http://imagerepo.pictionary.jpg","pictionary");

        Product saved_prd1=manager.persist(prd1);
        Product saved_prd2=manager.persist(prd2);

        Assertions.assertEquals(saved_prd1,productRepository.findByName(saved_prd1.getName()));
    }


    @Test
    public void shouldUpdateProductDetails()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product saved_prd1=manager.persist(prd1);

        saved_prd1.setQuantity(5);
        productRepository.save(saved_prd1);
        Assertions.assertEquals(5,manager.find(Product.class,saved_prd1.getId()).getQuantity());
    }

    @Test
    public void shouldDeleteProduct()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product saved_prd1=manager.persist(prd1);
        productRepository.delete(saved_prd1);
        Assertions.assertNull(manager.find(Product.class,saved_prd1.getId()));

    }
}