package com.thoughtworks.vapasi.gaminzo.controller;

import com.thoughtworks.vapasi.gaminzo.model.Product;
import com.thoughtworks.vapasi.gaminzo.repository.ProductRepository;
import com.thoughtworks.vapasi.gaminzo.service.ProductService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;

@AutoConfigureWebTestClient()
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WithMockUser("test@gmail.com")

public class ProductControllerTest {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    ProductRepository productRepository;


    @MockBean
    ProductService productService;


    private String url="/gaminzo/products";

    @Test
    public void shouldGetAllProducts()
    {
        Product prd1=new Product("Dominos",10,300,"http://imagerepo.dominos.jpg","dominos");
        Product prd2=new Product("Pictionary",15,600,"http://imagerepo.pictionary.jpg","pictionary");

        Mockito.when(productService.getProducts()).thenReturn(Arrays.asList(prd1,prd2));

        webTestClient.get().uri(url).exchange().
                expectStatus().isOk();
    }
}
