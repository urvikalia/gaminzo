package com.thoughtworks.vapasi.gaminzo.controller;

import com.thoughtworks.vapasi.gaminzo.model.CartItem;
import com.thoughtworks.vapasi.gaminzo.repository.CartItemRepository;
import com.thoughtworks.vapasi.gaminzo.service.CartItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient()
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CartItemControllerTest {

    @MockBean
    private CartItemRepository cartItemRepository;

    @MockBean
    private CartItemService cartItemService;

    @Autowired
    private WebTestClient webTestClient;

    private String url="/gaminzo/cart/";

    @Test
    public void shouldTestAddItemToCart()
    {
        CartItem cartItem = new CartItem(1,1,1);

        webTestClient.get().uri("/gaminzo/cart/additemtocart").attribute("productname","Uno")
                            .exchange().expectStatus().is3xxRedirection();
    }

}
