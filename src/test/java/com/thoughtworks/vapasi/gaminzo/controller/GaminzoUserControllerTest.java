package com.thoughtworks.vapasi.gaminzo.controller;

import com.thoughtworks.vapasi.gaminzo.config.SecurityConfiguration;
import com.thoughtworks.vapasi.gaminzo.repository.GaminzoUserRepository;
import com.thoughtworks.vapasi.gaminzo.service.GaminzoUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WithMockUser
public class GaminzoUserControllerTest {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    private GaminzoUserRepository gaminzoUserRepository;

    @MockBean
    private GaminzoUserService gaminzoUserService;

    @Autowired
    SecurityConfiguration securityConfiguration;

    @Test
    public void shouldRenderSignupPage()
    {
        webTestClient.get().uri("gaminzo/signup")
                .exchange()
                .expectStatus()
                .isOk().expectBody().equals("signup");

    }

    @Test
    public void shouldRenderLoginPage()
    {
        webTestClient.get().uri("gaminzo/login")
                .exchange()
                .expectStatus()
                .isOk().expectBody().equals("login");

    }

//    @Test
//    @WithMockUser("neeta")
//    public void shouldLogin()
//    {
//        webTestClient.post().uri("gaminzo/login")
//                .exchange()
//                .expectStatus()
//                .isOk().expectBody().equals("login");
//
//    }
}
